﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ExpenseManager3000Server.Models;

namespace ExpenseManager3000Server.Controllers
{
    [Authorize]
    public class TipoRendimentosController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/TipoRendimentos
        public IQueryable<TipoRendimento> GettiposRendimento()
        {
            return db.tiposRendimento;
        }

        // GET: api/TipoRendimentos/5
        [ResponseType(typeof(TipoRendimento))]
        public async Task<IHttpActionResult> GetTipoRendimento(int id)
        {
            TipoRendimento tipoRendimento = await db.tiposRendimento.FindAsync(id);
            if (tipoRendimento == null)
            {
                return NotFound();
            }

            return Ok(tipoRendimento);
        }

        // PUT: api/TipoRendimentos/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTipoRendimento(int id, TipoRendimento tipoRendimento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tipoRendimento.TipoRendimentoId)
            {
                return BadRequest();
            }

            db.Entry(tipoRendimento).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TipoRendimentoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TipoRendimentos
        [ResponseType(typeof(TipoRendimento))]
        public async Task<IHttpActionResult> PostTipoRendimento(TipoRendimento tipoRendimento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.tiposRendimento.Add(tipoRendimento);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = tipoRendimento.TipoRendimentoId }, tipoRendimento);
        }

        // DELETE: api/TipoRendimentos/5
        [ResponseType(typeof(TipoRendimento))]
        public async Task<IHttpActionResult> DeleteTipoRendimento(int id)
        {
            TipoRendimento tipoRendimento = await db.tiposRendimento.FindAsync(id);
            if (tipoRendimento == null)
            {
                return NotFound();
            }

            db.tiposRendimento.Remove(tipoRendimento);
            await db.SaveChangesAsync();

            return Ok(tipoRendimento);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TipoRendimentoExists(int id)
        {
            return db.tiposRendimento.Count(e => e.TipoRendimentoId == id) > 0;
        }
    }
}