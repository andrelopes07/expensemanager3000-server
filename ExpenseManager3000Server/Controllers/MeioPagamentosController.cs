﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ExpenseManager3000Server.Models;

namespace ExpenseManager3000Server.Controllers
{
    [Authorize]
    public class MeioPagamentosController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/MeioPagamentos
        public IQueryable<MeioPagamento> GetmeiosPagamento()
        {
            return db.meiosPagamento;
        }

        // GET: api/MeioPagamentos/5
        [ResponseType(typeof(MeioPagamento))]
        public async Task<IHttpActionResult> GetMeioPagamento(int id)
        {
            MeioPagamento meioPagamento = await db.meiosPagamento.FindAsync(id);
            if (meioPagamento == null)
            {
                return NotFound();
            }

            return Ok(meioPagamento);
        }

        // PUT: api/MeioPagamentos/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutMeioPagamento(int id, MeioPagamento meioPagamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != meioPagamento.MeioPagamentoId)
            {
                return BadRequest();
            }

            db.Entry(meioPagamento).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MeioPagamentoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MeioPagamentos
        [ResponseType(typeof(MeioPagamento))]
        public async Task<IHttpActionResult> PostMeioPagamento(MeioPagamento meioPagamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.meiosPagamento.Add(meioPagamento);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = meioPagamento.MeioPagamentoId }, meioPagamento);
        }

        // DELETE: api/MeioPagamentos/5
        [ResponseType(typeof(MeioPagamento))]
        public async Task<IHttpActionResult> DeleteMeioPagamento(int id)
        {
            MeioPagamento meioPagamento = await db.meiosPagamento.FindAsync(id);
            if (meioPagamento == null)
            {
                return NotFound();
            }

            db.meiosPagamento.Remove(meioPagamento);
            await db.SaveChangesAsync();

            return Ok(meioPagamento);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MeioPagamentoExists(int id)
        {
            return db.meiosPagamento.Count(e => e.MeioPagamentoId == id) > 0;
        }
    }
}